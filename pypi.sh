#!/bin/bash
#
# Generate pypi wheels universal package and upload
# in case anything goes wrong use:
# $twine check dist/*

py_cmd="python"
if ! hash python; then
    py_cmd="python3"
fi

if ! hash twine; then
    echo "Twine is not present and must be installed. Installing Twine with sudo apt install"
    sudo apt install twine
fi

# clean up
$py_cmd setup.py clean --all
rm -rf dist
rm -rf build
rm -rf *.egg-info

# build
$py_cmd setup.py sdist bdist_wheel

# deploy
twine upload --config-file ../env_var/secret.pypirc --verbose dist/*